-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2017 at 01:26 AM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aic`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `authors` text CHARACTER SET utf8 NOT NULL,
  `year` int(4) NOT NULL,
  `edition` int(11) NOT NULL,
  `summary` text CHARACTER SET utf8 NOT NULL,
  `publication_info` text CHARACTER SET utf8 NOT NULL,
  `ISBN` text CHARACTER SET utf8 NOT NULL,
  `length` text CHARACTER SET utf8 NOT NULL,
  `contributors` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_courses`
--

CREATE TABLE `book_courses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `college_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `college_name`) VALUES
(1, 'College of Business and Accountancy - Dean\'s Office'),
(2, 'College of Computer Studies - Dean\'s Office'),
(3, 'College of Humanities and Social Sciences - Dean\'s Office'),
(4, 'College of Nursing - Dean\'s Office'),
(5, 'College of Science and Engineering - Dean\'s Office');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_code` varchar(25) NOT NULL,
  `course_title` text NOT NULL,
  `department_id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_code`, `course_title`, `department_id`, `college_id`) VALUES
(1, 'ICST101', 'Computing Fundamental', 1, 2),
(2, 'ICST102', 'Computer Programming 1', 1, 2),
(3, 'ICST103', 'Computer Programming 2', 1, 2),
(4, 'ICST104', 'Discrete Structures', 1, 2),
(5, 'ICST201', 'Data Structures', 1, 2),
(6, 'ICST214', 'Object Oriented Programming', 1, 2),
(7, 'ICST217', 'Applied Statistics', 1, 2),
(8, 'ICST213', 'Database Systems', 1, 2),
(9, 'ICST251', 'Structure of Programming Languages', 1, 2),
(10, 'ICST105', 'Computer Organization and Machine Language Assembler ', 1, 2),
(11, 'ICST221', 'Systems Analysis and Design', 1, 2),
(12, 'ICST231', 'Web Development and Programming', 1, 2),
(13, 'ICST250', 'Theory of Automata and Formal Languages', 1, 2),
(14, 'ICST202', 'Algorithm Analysis and Design', 1, 2),
(15, 'ICST222', 'Software Engineering', 1, 2),
(16, 'ICST240', 'Operating Systems', 1, 2),
(17, 'ICST252', 'Compiler Design and Implementation', 1, 2),
(18, 'ICST241', 'Data Communications and Networking', 1, 2),
(19, 'ICST253', 'Modeling and Simulation Theory', 1, 2),
(20, 'ICST108', 'Professional Ethics', 1, 2),
(21, 'PHYS031', 'Principles of Electronics', 1, 2),
(22, 'PHYS032', 'Digital Design and Logic Switching', 1, 2),
(23, 'ICST203', 'Fundamentals of Management and Business', 1, 2),
(24, 'ICST106', 'Electronics and Computer Organization', 1, 2),
(25, 'ICST211', 'Database Management Systems 1', 1, 2),
(26, 'ICST230', 'Multimedia Systems', 1, 2),
(27, 'ICST212', 'Database Management Systems 2', 1, 2),
(28, 'ICST107', 'Technopreneurship', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `college_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dept_name`, `college_id`) VALUES
(1, 'Computer Science Department', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_number` int(11) DEFAULT NULL,
  `gbox` varchar(30) CHARACTER SET utf8 NOT NULL,
  `full_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_courses`
--

CREATE TABLE `user_courses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_list` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_courses`
--
ALTER TABLE `book_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `course_code` (`course_code`),
  ADD UNIQUE KEY `course_code_2` (`course_code`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gbox` (`gbox`);

--
-- Indexes for table `user_courses`
--
ALTER TABLE `user_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_courses`
--
ALTER TABLE `book_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `user_courses`
--
ALTER TABLE `user_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `book_courses`
--
ALTER TABLE `book_courses`
  ADD CONSTRAINT `book_courses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `book_courses_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `book_courses_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`);

--
-- Constraints for table `user_courses`
--
ALTER TABLE `user_courses`
  ADD CONSTRAINT `user_courses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
