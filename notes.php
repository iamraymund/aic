// NOTE:40 Clone git repo (bitbucket.org)
// SSH: git clone git@bitbucket.org:iamraymund/aic.git
// HTTPS: git clone https://iamraymund@bitbucket.org/iamraymund/aic.git

// NOTE: API is in /index.php/api
// TODO:210 Recommendation: Implement Basic OAuth

// NOTE:0 December 17-29, 2016: Completed all features and initial UI

// DONE:110 Add a table list of all added books with 'book_id' and 'course_code'
// NOTE:30 2 option to add book_course. If existing, add the 'book_id', otherwise add new book details
// DONE:90 Minimum of 5 books, 2 books must be within 3 year publication period
// DONE:100 Dashboard for notication (all users) if not yet reach both book requirements
// DONE:80 Make a format for authors
// DONE:140 Have an option to change role to chairperson / faculty
// DONE:150 Have an option to change role to faculty

// DONE:170 Make a single controller for every user
// DONE:160 Make a navigation for dean

// DONE:60 Make a reports table for every course together with the books added (dean)
// DONE:70 Make a reports table for every course together with the books added (chairperson)

// DONE:0 Put toast in all CRUD operations

1. yung account po na binigay satin (tho nalaman na po natin ung pass), tigcheck ko po kanina. hindi po siya nakaassign sa kahit anong designation type
Kaya for now, yung gbox acct na binigay po samin sa thesis ang ginagamit ko
2. Yung API na binigay po satin, by employee id ang input instead of gbox (gaya po ng sinabi ko sainyo kanina. tho gamit ko po now yung API sa thesis namin na via gbox po ang input)
3. Hindi kasama sa API kung anong college name nung nireturn na dept name (sa dean meron, sa chair and faculty wala)
If hindi po kaya gawan ng API yung #3, need ko po yung
  a. full list ng department column value para po magroup ko by college.
  a. full list ng college column value.
