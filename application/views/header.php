<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="Alvarez & Co of Ateneo Innovation Center">

    <title>Book Inventory</title>

  	<!--Let browser know website is optimized for mobile-->
  	<meta name="viewport" content="width=device-width, initial-scale=1"/>


    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-3.3.7/dist/css/bootstrap.min.css'); ?>">

    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-3.3.7/dist/css/bootstrap-theme.min.css'); ?>">

  	<!-- Selectize CSS -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/selectize/dist/css/selectize.default.css'); ?>" />

  	<!-- Bootstrap Table CSS -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap-table-1.11.0/dist/bootstrap-table.min.css'); ?>" />

    <!-- Simple Sidebar CSS -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/simple-sidebar.css'); ?>" />

    <!-- Toast -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toastr-2.1.3/toastr.min.css'); ?>" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">

</head>

<body>
