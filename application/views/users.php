<div id="page-wrapper">
	<div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" align="center">
                    Users
                </h1>
                
            </div>
        </div>
        
	</div>
    <!-- /.row -->
    <!-- Start Courses -->
    <br /><br />
    
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="container-fluid">
				<a href="#" type="button" class="btn btn-primary">Add New User</a><br /><br />            	
                <div class="panel panel-primary">
                    <div class="panel-heading header_design">
                        <h4 class="panel-title" style = "color:white;" align="center">Users</h4>
                    </div>
                
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered text-center" id="">
                            <thead>
                                <tr>
                                    <th class="text-center">ID Number</th>
                                    <th class="text-center">Full Name</th>
                                    <th class="text-center">Role</th>
                                    <th class="text-center" width="15%">Actions</th>
                                </tr>
                            </thead>
                        
                            <tbody id="">
                            	
                            	
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   	<!-- End Courses -->
</div>