<script>
// NOTE:10 Book active tab. This is found when you click "Add Book" button, '1' for New Book, '2' for Existing Book
$('#active_tab').val('1');

setTimeout(function () {
    $('#book').bootstrapTable('resetView');
}, 200);


$(document).ready(function(){
  // DONE:30 Recommendation: Improve this trigger for the anchor tag of the first course
  var value = $('ul.nav-sidebar li a:first').attr('value');
  $(".nav-sidebar li").removeClass("active");
  $('.nav-sidebar li').first().addClass('active');
  // If there is course assigned
  if(value != undefined){
    $('.course_code').val(value);
    $.ajax({
       type: "POST",
       url: "<?php echo site_url('faculty/courses_get_books');?>",
       data: {code : value},
       datatype: 'application/json',
       success: function(data){
          var obj = $.parseJSON(data);
          $('#books').bootstrapTable("destroy");
          $('#books').bootstrapTable({ data: obj });
          $('#books').bootstrapTable('resetView');
       }
     });
  }

  $('a.course').click(function(){
		var value = $(this).attr('value');
    $('.course_code').val(value);
    $(".nav-sidebar li").removeClass("active");
    $(this).closest('li').addClass('active');
    $.ajax({
			 type: "POST",
			 url: "<?php echo site_url('faculty/courses_get_books');?>",
			 data: {code : value},
       datatype: 'application/json',
			 success: function(data){
				 	var obj = $.parseJSON(data);
          $('#books').bootstrapTable("destroy");
          $('#books').bootstrapTable({ data: obj });
          $('#books').bootstrapTable('resetView');
			 }
		 });
  });

  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
  })

  $('ul#add_book li a').click(function (e) {
    var cur = $(this).attr('href');
    if(cur == '#existing'){
      // $('#title').prop('required', false);
      // $('#add_authors').prop('required', false);
      // $('#year').prop('required', false);
      // $('#edition').prop('required', false);
      // $('#book_id').prop('required', true);
      $('#active_tab').val('2');
    }else{
      // $('#title').prop('required', true);
      // $('#add_authors').prop('required', true);
      // $('#year').prop('required', true);
      // $('#edition').prop('required', true);
      // $('#book_id').prop('required', false);
      $('#active_tab').val('1');
    }
  });
});

// For the selectize
var $selectAdd = $('#add_authors').selectize({
  plugins: ['remove_button'],
  delimiter: ',',
  persist: false,
  create: function(input) {
      return {
          value: input,
          text: input
      }
  }
});

var $selectEdit = $('#update_authors').selectize({
  plugins: ['remove_button'],
  delimiter: ',',
  persist: false,
  create: function(input) {
      return {
          value: input,
          text: input
      }
  }
});
var selectizeControlEdit = $selectEdit[0].selectize;

function actionFormatter(value) {
    return [
      '<a class="btn btn-md btn-primary update" href="javascript:" role="button" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>',
      '<a class="btn btn-md btn-danger delete" href="javascript:" role="button" title="Delete"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('');
}

// update and delete events
window.actionEvents = {
    'click .update': function (e, value, row) {
      $('#updateStart').modal('show');
      var value = row.id;
      $.ajax({
         type: "POST",
         url: "<?php echo site_url('faculty/courses_single');?>",
         data: {id : value},
         dataType: 'json',
         success: function(data){
            $('#title_update').val(data.title);
            $('#year_update').val(data.year);
            $('#edition_update').val(data.edition);
            $('#summary_update').val(data.summary);
            $('#publication_info_update').val(data.publication_info);
            $('#ISBN_update').val(data.ISBN);
            $('#length_update').val(data.length);
            $('#contributors_update').val(data.contributors);

            selectizeControlEdit.clear(); // remove all selected values
            var obj = data.authors.split(',');
            for(i=0; i<obj.length; i++){
              selectizeControlEdit.addOption({value: obj[i],text:obj[i]});
              selectizeControlEdit.addItem(obj[i]);
            }
         }
       });
      $('#updateEnd').val(value);
    },
    'click .delete': function (e, value, row) {
      $('#deleteStart').modal('show');
      $('#deleteEnd').val(row.id);
    }
};

</script>
