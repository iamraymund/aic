<div class="container-fluid">
	<div class="container">
      <div class="row">
        <div class="col l12">
					<div class="table-responsive">
						<form method="post">
						<button type="button" id="toolbar" class="btn btn-primary btn-md" data-toggle="modal" data-backdrop='static' data-keyboard='false' data-target="#addStart">Add Courses</button>
						<table id="courses"
							data-toggle="table"
							data-toolbar="#toolbar"
	            data-show-toggle="true"
	            data-show-columns="true"
	            data-show-pagination-switch="true"
							data-pagination="true"
	            data-side-pagination="client"
	            data-classes="table table-striped"
							data-page-list="[5, 10, 25, 50, 100, ALL]"
	            data-search="true">
			        <thead>
			          <tr>
			              <th data-field="course_code" data-sortable="true">Course Code</th>
			              <th data-field="course_title" data-sortable="true">Course Title</th>
										<th data-field="action" data-formatter="actionFormatter" data-events="actionEvents" data-align="center">Action</th>
			          </tr>
			        </thead>
			      </table>
						</form>
				</div>
		   </div>
		</div>
	</div>
</div>

<!-- Add Course -->
<div id="addStart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
				<div class="modal-content">
	    			<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
	    				<h4 class="modal-title">New Course</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="course_code">Course Code:</label>
							  <input type="text" class="form-control" placeholder="Enter Course Code" name="course_code" required="true">
							</div>
							<div class="form-group">
								<label for="course_title">Course Title:</label>
							  <input type="text" class="form-control" placeholder="Enter Course Title" name="course_title" required="true">
							</div>
						</div>
						<div class="modal-footer">
					      <button type="submit" class="btn btn-primary" name="addEnd" value="addEnd">Add</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					  </div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Update Course -->
<div id="updateStart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
					<div class="modal-content">
		    			<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
		    				<h4 class="modal-title">Update Course</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="course_code_update">Course Code:</label>
								  <input type="text" class="form-control" placeholder="Enter Course Code" name="course_code_update" id="course_code_update" required="true">
								</div>
								<div class="form-group">
									<label for="course_title_update">Course Title:</label>
								  <input type="text" class="form-control" placeholder="Enter Course Title" name="course_title_update" id="course_title_update" required="true">
								</div>
							</div>
							<div class="modal-footer">
						      <button type="submit" class="btn btn-primary" id="updateEnd" name="updateEnd" value="updateEnd">Update</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						  </div>
					</div>
				</form>
		</div>
	</div>
</div>

<!-- Delete Course -->
<div id="deleteStart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
					<div class="modal-content">
		    			<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
		    				<h4 class="modal-title">Delete Course</h4>
							</div>
							<div class="modal-body">
								<p>Are you sure you want to delete it?</p>
							</div>
							<div class="modal-footer">
						      <button type="submit" class="btn btn-primary" id="deleteEnd" name="deleteEnd" value="deleteEnd">Delete</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						  </div>
					</div>
				</form>
		</div>
	</div>
</div>
