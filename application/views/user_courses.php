<div id="page-wrapper">
	<div class="container">
      <div class="row">
        <div class="col l12">
					<form method="post">
					<button type="button" id="toolbar" class="btn btn-primary btn-md" data-toggle="modal" data-backdrop='static' data-keyboard='false' data-target="#addStart">Add Course Assignment</button>
					<table id="user_courses"
						data-toggle="table"
						data-toolbar="#toolbar"
						data-show-toggle="true"
						data-show-columns="true"
						data-show-pagination-switch="true"
						data-side-pagination="client"
						data-pagination="true"
						data-classes="table table-striped"
						data-page-list="[5, 10, 25, 50, 100, ALL]"
						data-search="true">
		        <thead>
		          <tr>
		              <th data-field="gbox" data-sortable="true">Gbox Address</th>
		              <th data-field="full_name" data-sortable="true">Full Name</th>
		              <th data-field="course_list">Assigned Course</th>
									<th data-field="action" data-formatter="actionFormatter" data-events="actionEvents" data-align="center">Action</th>
		          </tr>
		        </thead>
		      </table>
				</form>

		   </div>
		</div>
	</div>
</div>

<!-- Add User Courses -->
<div id="addStart" class="modal fade">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">New User Course</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="email" class="form-control" placeholder="Enter GBOX email address" name="gbox" required="true">
							</div>
							<div class="form-group">
								<label for="add_courses">Assigned Course:</label>
								<select id="add_courses" name="courses[]" class="selectized" placeholder="Pick courses" multiple="multiple">
									<?php
				        		if($courses){
					        		foreach($courses as $row){
					        			echo '<option value="'.$row->course_code.'">' . $row->course_title . '</option>';					        		}
										}
									?>
								</select>
							</div>
							<div class="input-group">
								<input type="hidden" id="new_courses" name="courses" ></input>
							</div>

						</div>

						<div class="modal-footer">
								<button type="submit" class="btn btn-primary" name="addEnd" value="addEnd">Add</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Update User Courses -->
<div id="updateStart" class="modal fade">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Update User Course</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="email" class="form-control" placeholder="Enter GBOX email address" id="gbox_update" name="gbox_update" required="true">
							</div>
							<div class="form-group">
								<label for="edit_courses">Assigned Course:</label>
								<select id="edit_courses" name="courses[]" class="selectized" placeholder="Pick courses" multiple="multiple">
									<?php
				        		if($courses){
					        		foreach($courses as $row){
					        			echo '<option value="'.$row->course_code.'">' . $row->course_title . '</option>';					        		}
										}
									?>
								</select>
							</div>
							<div class="input-group">
								<input type="hidden" id="update_courses" name="courses_update" ></input>
							</div>

						</div>

						<div class="modal-footer">
								<button type="submit" class="btn btn-primary" id="updateEnd" name="updateEnd" value="updateEnd">Update</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Delete User Course -->
<div id="deleteStart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
					<div class="modal-content">
		    			<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
		    				<h4 class="modal-title">Delete User Course</h4>
							</div>
							<div class="modal-body">
								<p>Are you sure you want to delete it?</p>
							</div>
							<div class="modal-footer">
						      <button type="submit" class="btn btn-primary" id="deleteEnd" name="deleteEnd" value="deleteEnd">Delete</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						  </div>
					</div>
				</form>
		</div>
	</div>
</div>
