<footer class="footer">
  <!--div class="container">
    <div class="row col-lg-12">
        <h5 class="white-text">AdNU Library Course-Based Book Inventory System</h5>
        <p class="grey-text text-lighten-4">AdNU Book Inventory(ABI) is a web application for ADNU College faculties that allows them to add, edit, delete and view the recommended books, that their respective departments require them to list for their respective courses.</p>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © Copyright 2016 Ateneo de Naga University Developed by the Ateneo Innovation Center</a>
    </div>
  </div-->
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/plugins/jquery-3.1.1/jquery.min.js'); ?>" ></script>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url('assets/plugins/bootstrap-3.3.7/dist/js/bootstrap.min.js'); ?>"></script>

<!-- Bootstrap Table JS -->
<script src="<?php echo base_url('assets/plugins/bootstrap-table-1.11.0/dist/bootstrap-table.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-table-1.11.0/dist/extensions/export/bootstrap-table-export.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-table-1.11.0/dist/extensions/export/tableExport/tableExport.min.js'); ?>"></script>

<!-- Bootstrap Toast -->
<script src="<?php echo base_url('assets/plugins/toastr-2.1.3/toastr.min.js'); ?>"></script>

<!-- Selectize JS -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/selectize/dist/js/standalone/selectize.js'); ?>"></script>
