<div class="container-fluid">
  <div class="row">
    <div id="sidebar-wrapper" class="navbar-inverse">
      <ul class="nav nav-sidebar">
        <?php
          if(isset($course_assigned)){
            foreach($course_assigned as $row){
              $course_code = $row->course_code;
              $total_book = $book_count[$course_code][0];
              $latest_book = $book_count[$course_code][1];
              echo "<li><a role='button' class='course' value='$course_code'>$row->course_code - $row->course_title";
              if($total_book >= 5){
                echo " <span class='badge badge-success' title='$total_book / 5 Req. Total Books'>$total_book</span>";
              }else{
                echo " <span class='badge badge-danger' title='$total_book / 5 Req. Total Books'>$total_book</span>";
              }
              if($latest_book >= 2){
                echo " <span class='badge badge-success' title='$latest_book / 2 Req. Latest Books'>$latest_book</span></a></li>";
              }else{
                echo " <span class='badge badge-danger' title='$latest_book / 2 Req. Latest Books'>$latest_book</span></a></li>";
              }
            }
          }
          else{
            echo "<li><a>No course assigned!</a></li>";
          }
        ?>
      </ul>
    </div>
		<div id="page-content-wrapper">
        <?php
          if(isset($course_assigned)){
        ?>
        <div class="table-responsive">
          <form method="POST">
          <input type="hidden" name="course_code" class="course_code">
          <button type="button" id="toolbar" class="btn btn-primary btn-md" data-toggle="modal" data-backdrop='static' data-keyboard='false' data-target="#addStart">Add Book</button>
          <table id="books"
          data-toggle="table"
          data-toolbar="#toolbar"
          data-show-toggle="true"
          data-show-columns="true"
          data-show-pagination-switch="true"
          data-pagination="true"
          data-side-pagination="client"
          data-classes="table table-striped"
          data-page-list="[5, 10, 25, 50, 100, ALL]"
          data-search="true">
              <thead>
                <tr>
                  <th data-field="title" data-sortable="true">Title</th>
                  <th data-field="authors">Author/s</th>
                  <th data-field="year" data-sortable="true">Year</th>
                  <th data-field="edition" data-sortable="true">Edition</th>
                  <!-- <th data-field="summary">Summary</th> -->
                  <th data-field="publication_info">Publication Info</th>
                  <th data-field="ISBN" data-sortable="true">ISBN</th>
                  <th data-field="length">Length</th>
                  <!-- <th data-field="contributors">Contributors</th> -->
                  <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents" data-align="center">Action</th>
                </tr>
              </thead>
          </table>
        </form>
        </div>
        <?php
          }
          else{
            echo "Note: Ask your chairperson / dean to assign you to a course.";
          }
        ?>
      </div>
  </div>
</div>

<!-- Add Books -->
<div id="addStart" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="row">
	    	<form method="post">
				<div class="modal-content">
	    			<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">New Book</h4>
						</div>
						<div class="modal-body">
              <ul id="add_book" class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#new">New Book</a></li>
                <li><a data-toggle="tab" href="#existing">From Existing Book</a></li>
              </ul>
              <div class="tab-content">
                <div id="new" class="tab-pane fade in active">
                  <div class="row">
                    <div class="container-fluid">
                      <br />
                      <div class="col-lg-6">
          							<div class="form-group">
          							  <label for="title">Title:</label>
          							  <input type="text" class="form-control" placeholder="Enter Title" name="title" id="title">
          							</div>
          							<div class="form-group">
          							  <label for="authors">Authors:</label>
          							  <input type="text" class="selectized" placeholder="Enter Author/s (format: Firstname M.I. Lastname)" name="add_authors" id="add_authors">
                        </div>
                        <div class="form-group">
                          <label for="year">Year:</label>
                          <input type="number" class="form-control" placeholder="Enter Year" name="year" id="year">
                        </div>
                        <div class="form-group">
                          <label for="edition">Edition:</label>
                          <input type="number" class="form-control" placeholder="Enter Edition (e.g. 1, 2, 3)" name="edition" id="edition">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <!-- <div class="input-group">
                          <span class="input-group-addon">Summary</span>
                          <textarea class="form-control" placeholder="Enter Summary" name="summary"></textarea>
                        </div> -->
                        <div class="form-group">
                          <label for="publication_info">Publication Info:</label>
                          <input type="text" class="form-control" placeholder="Enter Publication Info" name="publication_info">
                        </div>
                        <div class="form-group">
                          <label for="title">ISBN:</label>
                          <input type="text" class="form-control" placeholder="Enter ISBN" name="ISBN">
                        </div>
                        <div class="form-group">
                          <label for="title">Length:</label>
                          <input type="text" class="form-control" placeholder="Enter Length" name="length">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="existing" class="tab-pane fade">
                  <div class="row">
                    <div class="container-fluid">
                      <br />
                      <div class="col-lg-12">
                        <div class="form-group">
                          <label for="book_id">Book ID:</label>
                            <input type="number" class="form-control" placeholder="Enter Book ID" id="book_id" name="book_id">
                          </div>
                          <div>See the list of books <a href="<?php echo site_url('faculty/books'); ?>" target="_blank">HERE</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
						</div>
						<div class="modal-footer">
                <input type="hidden" name="course_code" class="course_code">
                <input type="hidden" name="active_tab" id="active_tab">
					      <button type="submit" class="btn btn-primary" name="addEnd" value="addEnd">Add</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					  </div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Update Book -->
<div id="updateStart" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="row">
	    	<form method="post">
          <input type="hidden" name="course_code" class="course_code">

					<div class="modal-content">
		    			<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
		    				<h4 class="modal-title">Update Course</h4>
							</div>
              <div class="modal-body">
                <div class="row">
                  <div class="container-fluid">
                      <div class="col-lg-6">
          							<div class="form-group">
          							  <label for="title_update">Title:</label>
          							  <input type="text" class="form-control" placeholder="Enter Title" name="title_update" id="title_update" required="true">
          							</div>
                        <div class="form-group">
          							  <label for="authors_update">Authors:</label>
          							  <input type="text" class="selectized" placeholder="Enter Author/s  (format: First Name M.I. Lastname)" name="update_authors" id="update_authors" required="true">
          							</div>
                        <div class="form-group">
          							  <label for="year_update">Year:</label>
                          <input type="number" class="form-control" placeholder="Enter Year" name="year_update" id="year_update" required="true">
                        </div>
                        <div class="form-group">
          							  <label for="edition_update">Edition:</label>
                          <input type="number" class="form-control" placeholder="Enter Edition (e.g. 1, 2, 3)" name="edition_update" id="edition_update" required="true">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <!-- <div class="input-group">
                          <span class="input-group-addon">Summary</span>
                          <textarea class="form-control" placeholder="Enter Summary" name="summary_update" id="summary_update"></textarea>
                        </div> -->
                        <div class="form-group">
          							  <label for="publication_info_update">Publication Info:</label>
                          <input type="text" class="form-control" placeholder="Enter Publication Info" name="publication_info_update" id="publication_info_update">
                        </div>
                        <div class="form-group">
          							  <label for="ISBN_update">ISBN:</label>
                          <input type="text" class="form-control" placeholder="Enter ISBN" name="ISBN_update" id="ISBN_update">
                        </div>
                        <div class="form-group">
          							  <label for="length_update">Length:</label>
                          <input type="text" class="form-control" placeholder="Enter Length" name="length_update" id="length_update">
                        </div>
                        <!-- <div class="input-group">
                          <span class="input-group-addon">Contributors</span>
                          <input type="text" class="form-control" placeholder="Enter Contributors" name="contributors_update" id="contributors_update">
                        </div> -->
                      </div>
                    </div>
                  </div>
  						</div>
							<div class="modal-footer">
						      <button type="submit" class="btn btn-primary" id="updateEnd" name="updateEnd" value="updateEnd">Update</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						  </div>
					</div>
				</form>
		</div>
	</div>
</div>

<!-- Delete Book -->
<div id="deleteStart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="row">
	    	<form method="post">
          <input type="hidden" name="course_code" class="course_code">
					<div class="modal-content">
		    			<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
		    				<h4 class="modal-title">Delete Course</h4>
							</div>
							<div class="modal-body">
								<p>Are you sure you want to delete it?</p>
							</div>
							<div class="modal-footer">
						      <button type="submit" class="btn btn-primary" id="deleteEnd" name="deleteEnd" value="deleteEnd">Delete</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						  </div>
					</div>
				</form>
		</div>
	</div>
</div>
