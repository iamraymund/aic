<div class="main-head-index">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
          <h1>AdNU Library Course-Based Book Inventory System</h1>
          <p></p><h4>AdNU Library Course-Based Book Inventory System is a web application for ADNU College faculties that allows them to add, edit, delete and view the recommended books, that their respective departments require them to list for their respective courses.<br><br></h4><p></p>
      </div>
    </div>
  </div>
</div>

<div class="text-center grey-pattern-box">
  <div class="container">
  	<div class="row">
      <div class="col-md-4">
        <img class="logo" src="<?php echo base_url('assets/images/adnu-logo.png'); ?>" alt="AdNU Logo">
        <p>Project for Ateneo de Naga University</p>
      </div>
      <div class="col-md-4">
        <img class="logo" src="<?php echo base_url('assets/images/mis-logo.png'); ?>" alt="MIS Logo">
        <p>Maintained by Management Information Systems (MIS)</p>
      </div>
      <div class="col-md-4">
        <img class="logo" src="<?php echo base_url('assets/images/aic-logo.png'); ?>" alt="AIC Logo">
        <p>Developed by Ateneo Innovation Center (AIC)</p>
      </div>
	</div>
  </div>
</div>
<div class="text-center home-footer" >
  Copyright © <?php echo date('Y'); ?> Ateneo de Naga University
</div>
