<div class="container-fluid">
    <div class="row col-lg-12">
				<div class="bs-callout bs-callout-info">
					<h4>Requirements:</h4>
					<p>Minimum of five (5) books and two (2) books must be within 3 year publication period (e.g. current year is 2014, then books published within 2011 - 2014 are still good)</p>
				</div>
				<div class="row">
					<?php
						if($course_list){
							$ctr = 1;
							$size = sizeof($course_list);
							if($size % 2 == 0){
								$half = $size / 2;
							}else{
								$half = $size / 2 + 1;
							}
              $half = (int)$half;

							echo "<div class='col-md-6'><ul class='list-group'>";
							foreach($course_list as $row){
								// $tmp = $row->id;
								$total_book = $book_count[$row->id][0];
								$latest_book = $book_count[$row->id][1];
								if($ctr == $half + 1){
									// echo "<script>console.log('hey');</script>";
									echo "</ul></div><div class='col-md-6 col-sm-12'><div class='list-group'>";
								}
								echo  '<li class="list-group-item">' . $row->course_code . " - " . $row->course_title;
								if($latest_book >= 2){
									echo " <span class='badge badge-success' title='$latest_book / 2 Req. Latest Books'>$latest_book</span>";
								}else{
									echo " <span class='badge badge-danger' title='$latest_book / 2 Req. Latest Books'>$latest_book</span>";
								}
								if($total_book >= 5){
									echo "<span class='badge badge-success' title='$total_book / 5 Req. Total Books'>$total_book</span></li>";
								}else{
									echo "<span class='badge badge-danger' title='$total_book / 5 Req. Total Books'>$total_book</span></li>";
								}
								$ctr++;
							}
							echo "</div></div>";
						}
					?>
			</div>
		</div>
	</div>
