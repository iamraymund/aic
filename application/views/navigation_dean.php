<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo site_url('dean'); ?>">AdNU Book Inventory</a>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li <?php if ( $this->uri->uri_string() == 'dean' ) echo "class='active'"; ?>><a href="<?php echo site_url('dean');?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
      <li <?php if ( $this->uri->uri_string() == 'dean/reports' ) echo "class='active'"; ?>><a href="<?php echo site_url('dean/reports');?>"><span class="glyphicon glyphicon-ok-sign"></span> Reports</a></li>
    </ul>
		<div class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Change Role <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('chairperson'); ?>">As Chairperson</a></li>
          <li><a href="<?php echo site_url('faculty'); ?>">As Faculty</a></li>
        </ul>
      </li>

      <a href="<?php echo site_url('sign_out');?>" class="navbar-btn btn sign-out" role="button">Sign Out</a>
    </div>
  </div><!--/.nav-collapse -->
</nav>
