<script>

var $table = $('#courses').bootstrapTable({ url: "<?php echo site_url('chairperson/course_list_select_courses');?>"});

setTimeout(function () {
    $table.bootstrapTable('resetView');
}, 200);

function actionFormatter(value) {
    return [
      '<a class="btn btn-md btn-primary update" href="javascript:" role="button" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>',
      '<a class="btn btn-md btn-danger delete" href="javascript:" role="button" title="Delete"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('');
}

// update and delete events
window.actionEvents = {
    'click .update': function (e, value, row) {
				$('#updateStart').modal('show');
				var value = row.id;
				$.ajax({
					 type: "POST",
					 url: "<?php echo site_url('chairperson/course_list_single');?>",
					 data: {id : value},
					 dataType: 'json',
					 success: function(data){
							var json_str = JSON.stringify(data);
							var obj = JSON.parse(json_str);
							$('#course_code_update').val(obj[0]['course_code']);
							$('#course_title_update').val(obj[0]['course_title']);
					 }
				 });
				$('#updateEnd').val(value);
    },
    'click .delete': function (e, value, row) {
				$('#deleteStart').modal('show');
				$('#deleteEnd').val(row.id);
    }
};
</script>
