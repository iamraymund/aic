<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
  			<span class="sr-only">Toggle navigation</span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
			</button>
      <a class="navbar-brand" href="<?php echo site_url('faculty'); ?>">AdNU Book Inventory</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li <?php if ( $this->uri->uri_string() == 'faculty' ) echo "class='active'"; ?>><a href="<?php echo site_url('faculty'); ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li <?php if ( $this->uri->uri_string() == 'faculty/books' ) echo "class='active'"; ?>><a href="<?php echo site_url('faculty/books'); ?>"><span class="glyphicon glyphicon-book"></span> Books</a></li>
				<li><a href="http://webopac.adnu.edu.ph/" target="_blank"><span class="glyphicon glyphicon-cloud"></span> OPAC Website</a></li>
      </ul>
      <div class="nav navbar-nav navbar-right">
        <?php
          if($this->session->userdata('original_position') != 'Faculty'){
            echo<<<HTML
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Change Role <span class="caret"></span></a>
              <ul class="dropdown-menu">
HTML;
                  // if current user is originally a chairperson
                  if($this->session->userdata('original_position') == 'Chairperson'){
                      echo "<li><a href='" . site_url('chairperson') . "'>As Chairperson</a></li>";
                  }
                  // if current user is originally a dean
                  else{
                    echo "<li><a href='" . site_url('dean') . "'>As Dean</a></li>";
                    echo "<li><a href='" . site_url('chairperson') . "'>As Chairperson</a></li>";
                  }
            echo<<<HTML
              </ul></li>
HTML;
          }
         ?>
        <a href="<?php echo site_url('sign_out');?>" class="navbar-btn btn sign-out" role="button">Sign Out</a>
      </div>
    </div><!--/.nav-collapse -->
  </div>
</nav>
