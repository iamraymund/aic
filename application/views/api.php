<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url('api#get_book_detail'); ?>">get_book_detail</a></li>
        <li><a href="<?php echo site_url('api#get_book_courses'); ?>">get_book_courses</a></li>
        <li><a href="<?php echo site_url('api#get_user_courses'); ?>">get_user_courses</a></li>
        <li><a href="<?php echo site_url('api#get_user_details'); ?>">get_user_details</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>

<div class="container-fluid">
  <div id="get_book_detail">
    <h4>Method</h4>
    get_book_details
    <h4>Description</h4>
    Retrieve all book details
    <h4>URL</h4>
    http://localhost/AIC/index.php/api/get_book_details
    <h4>Parameters</h4>
    <ul>
      <li>book_id : From get_book_courses method</li>
    </ul>
    <h4>Return</h4>
    <ul>
      <li>result (boolean)</li>
        <ul>
          <li>true - API successfully executed</li>
          <li>false - API error</li>
        </ul>
      <li>title string</li>
      <li>authors string</li>
      <li>year int(4)</li>
      <li>edition int(11)</li>
      <li>summary string</li>
      <li>publication_info string</li>
      <li>ISBN string</li>
      <li>length string</li>
      <li>contributors string</li>
      <li>created_by int(11) - The user id of the one who created the book</li>
      <li>last_modified_by int(11) - The user id of the one who last modified the book</li>
    </ul>
  </div>
  <hr>
  <div id="get_book_courses">
    <h4>Method</h4>
    get_book_courses
    <h4>Description</h4>
    Retrieve all the books under specific course
    <h4>URL</h4>
    http://localhost/AIC/index.php/api/get_book_courses
    <h4>Parameters</h4>
    <ul>
      <li>course_code : From get_book_courses method</li>
    </ul>
    <h4>Return</h4>
    <ul>
      <li>result (boolean)</li>
        <ul>
          <li>true - API successfully executed</li>
          <li>false - API error</li>
        </ul>
      <li>book_id array - Array of all book id</li>
    </ul>
  </div>
  <hr>
  <div id="get_user_courses">
    <h4>Method</h4>
    get_user_courses
    <h4>Description</h4>
    Retrieve all courses handled by specific user
    <h4>URL</h4>
    http://localhost/AIC/index.php/api/get_book_details
    <h4>Parameters</h4>
    <ul>
      <li>gbox : gbox email address</li>
      OR
      <li>id_number : Faculty id number
    </ul>
    <h4>Return</h4>
    <ul>
      <li>result (boolean)</li>
        <ul>
          <li>true - API successfully executed</li>
          <li>false - API error</li>
        </ul>
      <li>course_code array - Array of all course code</li>
    </ul>
  </div>
  <hr>
  <div id="get_user_details">
  <h4>Method</h4>
  get_user_details
  <h4>Description</h4>
  Retrieve all user details
  <h4>URL</h4>
  http://localhost/AIC/index.php/api/get_user_details
  <h4>Parameters</h4>
  <ul>
    <li>user_id : User ID</li>
  </ul>
  <h4>Return</h4>
  <ul>
    <li>result (boolean)</li>
      <ul>
        <li>true - API successfully executed</li>
        <li>false - API error</li>
      </ul>
    <li>id_number int(11)</li>
    <li>gbox string</li>
    <li>full_name string</li>
    <li>dept_name string</li>
    <li>college_name stirng</li>
  </ul>
</div>
</div>
</body>
</html>
