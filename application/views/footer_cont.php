<?php
  if($this->session->flashdata('add') == 'success'){
    echo "<script>$(document).ready(function(){toastr.success('Successfully added!');});</script>";
  }
  else if($this->session->flashdata('add') == 'error'){
    echo "<script>$(document).ready(function(){toastr.error('Error: Not successfully added!');});</script>";
  }
  else if($this->session->flashdata('update') == 'success'){
    echo "<script>$(document).ready(function(){toastr.success('Successfully updated!');});</script>";
  }
  else if($this->session->flashdata('update') == 'error'){
    echo "<script>$(document).ready(function(){toastr.error('Error: Not successfully updated!');});</script>";
  }
  else if($this->session->flashdata('delete') == 'success'){
    echo "<script>$(document).ready(function(){toastr.success('Successfully deleted!');});</script>";
  }
  else if($this->session->flashdata('delete') == 'error'){
    echo "<script>$(document).ready(function(){toastr.error('Error: Not successfully deleted!');});</script>";
  }
  else if($this->session->flashdata('sign_in' == 'token_error')){
    echo "<script>$(document).ready(function(){toastr.error('Error: Problem with Google Auth Access Token');});</script>";
  }
  else if($this->session->flashdata('sign_in' == 'gbox_error')){
    echo "<script>$(document).ready(function(){toastr.error('Error: Not a valid gbox account');});</script>";
  }
  else if($this->session->flashdata('sign_in' == 'api_error')){
    echo "<script>$(document).ready(function(){toastr.error('Error: No return data from API');});</script>";
  }
  else if($this->session->flashdata('sign_in' == 'employee_error')){
    echo "<script>$(document).ready(function(){toastr.error('Error! No employee with given gbox or no email supplied');});</script>";
  }
?>
</body>
</html>
