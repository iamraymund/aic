<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url('chairperson'); ?>">AdNU Book Inventory</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li <?php if ( $this->uri->uri_string() == 'chairperson' ) echo "class='active'"; ?>><a href="<?php echo site_url('chairperson') ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li <?php if ( $this->uri->uri_string() == 'chairperson/course-list' ) echo "class='active'"; ?>><a href="<?php echo site_url('chairperson/course-list') ?>"><span class="glyphicon glyphicon-list"></span> Courses</a></li>
        <li <?php if ( $this->uri->uri_string() == 'chairperson/user-courses' ) echo "class='active'"; ?>><a href="<?php echo site_url('chairperson/user-courses') ?>"><span class="glyphicon glyphicon-briefcase"></span> Course Assignment</a></li>
        <li <?php if ( $this->uri->uri_string() == 'chairperson/reports' ) echo "class='active'"; ?>><a href="<?php echo site_url('chairperson/reports') ?>"><span class="glyphicon glyphicon-ok-sign"></span> Reports</a></li>
      </ul>
			<div class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Change Role <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php
              // if current user is originally a chairperson
              if($this->session->userdata('original_position') == 'Chairperson'){
                  echo "<li><a href='" . site_url('faculty') . "'>As Faculty</a></li>";
              }
              // if current user is originally a dean
              else{
                echo "<li><a href='" . site_url('dean') . "'>As Dean</a></li>";
                echo "<li><a href='" . site_url('faculty') . "'>As Faculty</a></li>";
              }
            ?>
          </ul>
        </li>
        <a href="<?php echo site_url('sign_out');?>" class="navbar-btn btn sign-out" role="button">Sign Out</a>
			</div>
    </div><!--/.nav-collapse -->
</nav>
