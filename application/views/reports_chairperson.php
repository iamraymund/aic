<div class="container-fluid">
  <div class="row">
		<div id="page-content-wrapper">
        <div class="table-responsive">
          <table id="book_courses"
          data-toggle="table"
          data-show-toggle="true"
          data-show-columns="true"
          data-show-export="true"
          data-show-pagination-switch="true"
          data-pagination="true"
          data-side-pagination="client"
          data-classes="table table-striped"
          data-page-list="[5, 10, 25, 50, 100, ALL]"
          data-search="true">
              <thead>
                <tr>
                  <th data-field="course_code">Course Code</th>
                  <th data-field="course_title">Course Title</th>
                  <th data-field="title">Book Title</th>
                  <th data-field="authors">Author/s</th>
                  <th data-field="year">Year</th>
                  <th data-field="edition">Edition</th>
                  <!-- <th data-field="summary">Summary</th> -->
                  <th data-field="publication_info">Publication Info</th>
                  <th data-field="ISBN">ISBN</th>
                  <th data-field="length">Length</th>
                  <!-- <th data-field="contributors">Contributors</th> -->
                </tr>
              </thead>
          </table>
        </div>
      </div>
  </div>
</div>
