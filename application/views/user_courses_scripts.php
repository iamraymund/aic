<script>

var $table = $('#user_courses').bootstrapTable({ url: "<?php echo site_url('chairperson/user_courses_select_user_courses');?>"});

setTimeout(function () {
	$table.bootstrapTable('resetView');
}, 200);

var $selectAdd = $('#add_courses').selectize({
	plugins: ['remove_button'],
	persist: false,
	maxItems: null,
	valueField: 'code',
	labelField: 'title',
	searchField: ['title', 'code'],
	render: {
			item: function(item, escape) {
					return '<div>' +
							(item.title ? '<span class="title">' + escape(item.title) + '</span> (' : '') +
							(item.code ? '<span class="code">' + escape(item.code) + '</span>)' : '') +
					'</div>';
			},
			option: function(item, escape) {
					var label = item.title || item.code;
					var caption = item.title ? item.code : null;
					return '<div>' +
							'<span style="color: #000;font-size: 12px;font-weight: bold;">' + escape(label) + '</span><br />' +
							(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
					'</div>';
			}
	}
});

var $selectEdit = $('#edit_courses').selectize({
	plugins: ['remove_button'],
	persist: false,
	maxItems: null,
	valueField: 'code',
	labelField: 'title',
	searchField: ['title', 'code'],
	render: {
			item: function(item, escape) {
					return '<div>' +
							(item.title ? '<span class="title">' + escape(item.title) + '</span> (' : '') +
							(item.code ? '<span class="code">' + escape(item.code) + '</span>)' : '') +
					'</div>';
			},
			option: function(item, escape) {
					var label = item.title || item.code;
					var caption = item.title ? item.code : null;
					return '<div>' +
							'<span class="label" style="color: #000;">' + escape(label) + '</span><br />' +
							(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
					'</div>';
			}
	}
});

var selectizeControlAdd = $selectAdd[0].selectize;
selectizeControlAdd.on('change', function() {
		var value = selectizeControlAdd.getValue();
		$('#new_courses').val(value);
});

var selectizeControlEdit = $selectEdit[0].selectize;
selectizeControlEdit.on('change', function() {
		var value = selectizeControlEdit.getValue();
		$('#update_courses').val(value);
});


function actionFormatter(value) {
    return [
      '<a class="btn btn-md btn-primary update" href="javascript:" role="button" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>',
      '<a class="btn btn-md btn-danger delete" href="javascript:" role="button" title="Delete"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('');
}

// update and delete events
window.actionEvents = {
    'click .update': function (e, value, row) {
				$('#updateStart').modal('show');
				var value = row.id;
				$.ajax({
					 type: "POST",
					 url: "<?php echo site_url('chairperson/user_courses_single');?>",
					 data: {id : value},
					 success: function(data){
						 	var obj = $.parseJSON(data);
							$('#gbox_update').val(obj.gbox);

							selectizeControlEdit.clear(); // remove all selected values
							var obj = obj.course_list.split(',');
							for(i=0; i<obj.length; i++){
								selectizeControlEdit.addItem(obj[i]);
							}
					 }
				});
				$('#updateEnd').val(value);
    },
    'click .delete': function (e, value, row) {
				$('#deleteStart').modal('show');
				$('#deleteEnd').val(row.id);
    }
};
</script>
