<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dean extends CI_Controller{
			public function __construct(){
				parent::__construct();
				$this->load->model('dean_model');
			}
			public function index(){
    		// If a dean is logged in
    		if($this->session->userdata('original_position') == 'Dean')
				{
					$this->session->set_userdata('position', 'Dean');
					$course_list = $this->dean_model->home($this->session->userdata('college_id'));
					// DONE:130 Fetch the total of books added
					// DONE:120 Fetch latest books per subject
					$book_count = array();
					foreach($course_list as $row){
						$total = $this->dean_model->book_count($row->id);
						$latest_arr = $this->dean_model->book_count_latest($row->id);
						$latest = 0;
						$current_year = date("Y");
						foreach($latest_arr as $tmp){
							$latest_tmp = $this->dean_model->get_book_year($tmp->book_id);
							if($current_year - $latest_tmp <= 3){
								$latest++;
							}
						}
						$book_count[$row->id] = array($total, $latest);
					}
					$data['course_list'] = $course_list;
					$data['book_count'] = $book_count;
					$this->load->view('header');
					$this->load->view('navigation_dean');
					$this->load->view('home_dean', $data);
					$this->load->view('footer');
					$this->load->view('footer_cont');
				}
				else
				{
					redirect(site_url().'/sign_out', 'refresh');
				}
    	}
			/*
			 * For Dean's Reports menu
			*/
			public function reports(){
				// If a dean is logged in
				if($this->session->userdata('position') == 'Dean')
				{
					$this->load->view('header');
					$this->load->view('navigation_dean');
					$this->load->view('reports_dean');
					$this->load->view('footer');
					$this->load->view('reports_dean_scripts');
					$this->load->view('footer_cont');
				}
				else
				{
					redirect(site_url().'/sign_out', 'refresh');
				}
			}
			public function get_reports(){
				$course_list = $this->dean_model->home($this->session->userdata('college_id'));
				$book_courses = array();
				// traverse each course
				foreach($course_list as $row){
					$assigned_book = $this->dean_model->book_count_latest($row->id);
					$flag = false;
					foreach($assigned_book as $tmp){
						$book_details = $this->dean_model->get_book_details($tmp->book_id);
						if(!$flag){
							array_push($book_courses, array(
								'course_code'=>$row->course_code,
								'course_title'=>$row->course_title,
								'title'=>$book_details->title,
								'authors'=>$book_details->authors,
								'year'=>$book_details->year,
								'edition'=>$book_details->edition,
								'summary'=>$book_details->summary,
								'publication_info'=>$book_details->publication_info,
								'ISBN'=>$book_details->ISBN,
								'length'=>$book_details->length,
								'contributors'=>$book_details->contributors));
							$flag = true;
						}
						else{
							array_push($book_courses, array(
								'title'=>$book_details->title,
								'authors'=>$book_details->authors,
								'year'=>$book_details->year,
								'edition'=>$book_details->edition,
								'summary'=>$book_details->summary,
								'publication_info'=>$book_details->publication_info,
								'ISBN'=>$book_details->ISBN,
								'length'=>$book_details->length,
								'contributors'=>$book_details->contributors));
						}
					}
				}
				// $tmp = $book_courses[0][2];
				// echo "<script>console.log('$tmp');</script>";
				echo json_encode($book_courses);
			}
    }
?>
