<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Faculty extends CI_Controller{
			public function __construct(){
				parent::__construct();
				$this->load->model('courses_model');
				$this->load->model('books_model');
			}
			/*
			* For Faculty's Courses menu
			*/
			public function index(){
				// if logged in
				$position = $this->session->userdata('original_position');
				if($position == 'Dean' || $position == 'Chairperson' || $position == 'Faculty')
				{
					$this->session->set_userdata('position', 'Faculty');
					$gbox = $this->session->userdata('gbox');
					$user_id = $this->courses_model->get_id($gbox);
					$this->session->set_userdata('user_id', $user_id);
					$course_list = $this->courses_model->select_course($user_id);

					$this->load->view('header');
					$this->load->view('navigation_faculty');

					$arr = explode(',', $course_list);
					$hm = sizeof($arr);
					if(!empty($arr[0])){
						// echo "<script>console.log('$hm');</script>";
						$data['course_assigned'] = $this->courses_model->map_course_code($arr);

						$book_count = array();
						for($i=0; $i<sizeof($arr); $i++){
							$id = $this->courses_model->get_course_id($arr[$i]);
							$total = $this->courses_model->book_count($id);
							$latest_arr = $this->courses_model->book_count_latest($id);
							$latest = 0;
							$current_year = date("Y");
							foreach($latest_arr as $tmp){
								$latest_tmp = $this->courses_model->get_book_year($tmp->book_id);
								// 3 corresponds to 3 year publication period
								if($current_year - $latest_tmp <= 3){
									$latest++;
								}
							}
							$book_count[$arr[$i]] = array($total, $latest);
						}
						$data['book_count'] = $book_count;
						$this->load->view('courses', $data);
					}
					else{
						//$data['no_course_assigned'] = TRUE;
						$this->load->view('courses');
					}
					$this->load->view('footer');
					$this->load->view('courses_scripts');
					$this->load->view('footer_cont');

					if($this->input->post('addEnd')){
						$course_code = html_escape($this->input->post('course_code'));
						// Insert new books (not yet existing)
						if($this->input->post('active_tab') == '1'){
							$data = array(
								'title' => html_escape($this->input->post('title')),
								'authors' => html_escape($this->input->post('add_authors')),
								'year' => html_escape($this->input->post('year')),
								'edition' => html_escape($this->input->post('edition')),
								// 'summary' => html_escape($this->input->post('summary')),
								'publication_info' => html_escape($this->input->post('publication_info')),
								'ISBN' => html_escape($this->input->post('ISBN')),
								'length' => html_escape($this->input->post('length')),
								// 'contributors' => html_escape($this->input->post('contributors')),
								'created_by' => $user_id,
								'last_modified_by' => $user_id
							);
							$this->courses_model->insert($data, $user_id, $course_code);
						}
						else{
							// DONE:180 Insert new books (existing)
							$this->courses_model->insert_existing($user_id, $course_code, $this->input->post('book_id'));
						}
						redirect('faculty');
					}

					// Delete course
					if($delete = html_escape($this->input->post('deleteEnd'))){
						$course_code = html_escape($this->input->post('course_code'));
						$this->courses_model->delete($delete, $user_id, $course_code);
						redirect('faculty');
					}

					// Update course
					if($update = html_escape($this->input->post('updateEnd'))){
						$data = array(
							'title' => html_escape($this->input->post('title_update')),
							'authors' => html_escape($this->input->post('update_authors')),
							'year' => html_escape($this->input->post('year_update')),
							'edition' => html_escape($this->input->post('edition_update')),
							// 'summary' => html_escape($this->input->post('summary_update')),
							'publication_info' => html_escape($this->input->post('publication_info_update')),
							'ISBN' => html_escape($this->input->post('ISBN_update')),
							'length' => html_escape($this->input->post('length_update')),
							// 'contributors' => html_escape($this->input->post('contributors_update')),
							'last_modified_by' => $user_id
						);
						$this->courses_model->update($data, $update);
						redirect('faculty');
					}
				}
				else
				{
					redirect(site_url().'/sign_out', 'refresh');
				}
    	}

			// Get the data for the books that needs to be edited
			public function courses_single(){
				if($id = html_escape($this->input->post('id'))){
					echo json_encode($this->courses_model->get($id));
				}
			}

			public function courses_get_books(){
					if($code = html_escape($this->input->post('code'))){
						echo json_encode($this->courses_model->select_books($code));
					}
			}

			/*
			 * For Faculty's Books menu
			*/
			public function books(){
				// if a faculty is logged in
    		if($this->session->userdata('position') == 'Faculty')
				{
					$this->load->view('header');
					$this->load->view('navigation_faculty_modified');
					$this->load->view('books');
					$this->load->view('footer');
					$this->load->view('books_scripts');
					$this->load->view('footer_cont');
				}
				else
				{
					redirect(site_url().'/sign_out', 'refresh');
				}
			}
			public function books_get_books(){
				echo json_encode($this->books_model->select_books());
			}

    }
?>
