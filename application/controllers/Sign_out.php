<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Sign_out extends CI_Controller
	{
		public function index()
		{

			//loading session library
			$this->load->library('session');

			//removing session data
			$this->session->sess_destroy();

			// redirect to sign in page
			redirect(site_url().'/home', 'refresh');
		}
	}
?>
