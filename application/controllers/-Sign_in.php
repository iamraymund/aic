<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Sign_in extends CI_Controller
	{
		public function index()
		{
			$this->load->view('header');
			$this->load->view('navigation');
			$this->load->view('footer');
			$this->load->view('footer_cont');

			//adding data to session
			$this->session->set_userdata('gbox', 'ralvarez@gbox.adnu.edu.ph');
			$this->session->set_userdata('name', 'Raymund Edgar Alvarez');
			$this->session->set_userdata('college', 'Computer Studies');
			$this->session->set_userdata('department_id', '1');
			$this->session->set_userdata('department', 'Computer Science');
			$this->session->set_userdata('position', 'Chairperson');
			$this->session->set_userdata('id_number', '201210104');

			if($this->session->userdata('position') == 'Dean')
			{
				redirect(site_url().'/dean', 'refresh');
			}
			else if($this->session->userdata('position') == 'Chairperson')
			{
				redirect(site_url().'/course-list', 'refresh');
			}
			else
			{
				redirect(site_url().'/courses', 'refresh');
			}

		}
	}
?>
