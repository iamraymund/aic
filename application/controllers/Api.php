<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Api extends CI_Controller{
		public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
    }
    public function index(){
      $this->load->view('header');
      $this->load->view('api');
      $this->load->view('footer');
      $this->load->view('footer_cont');
    }
    // DONE:20 Get book details api
    public function get_book_details(){
      if($id = $this->input->get('book_id')){
        $result = $this->api_model->get_book_details($id);
        $result = array('result'=>true, 'return'=> $result);
      }
      else{
        $result = array('result'=>false);
      }
      echo json_encode($result);
    }
    // DONE:10 Get book courses api
    public function get_book_courses(){
      if($code = $this->input->get('course_code')){
        $result = $this->api_model->get_book_courses($code);
        $book_id = array();
        // Query error
        if($result == FALSE){
          $result = array('result'=>false);
        }
        else{
          foreach($result as $row){
            array_push($book_id, $row->book_id);
          }
          $result = array('result'=>true, 'book_id'=> $book_id);
        }
      }
      else{
        $result = array('result'=>false);
      }
      echo json_encode($result);
    }
    // DONE:40 Get user courses api
    public function get_user_courses(){
      $gbox = $this->input->get('gbox');
      $id_number = $this->input->get('id_number');
      if($gbox || $id_number){
        if($gbox){
          $result = $this->api_model->get_user_courses_gbox($gbox);
        }
        else{
          $result = $this->api_model->get_user_courses_id($id_number);
        }
        // Query error
        if($result == FALSE){
          $result = array('result'=>false);
        }
        else{
          $arr = explode(',', $result);
          $result = array('result'=>true, 'course_code'=> $arr);
        }
      }
      else{
        $result = array('result'=>false);
      }
      echo json_encode($result);
    }
    // DONE:50 Get user details api
    public function get_user_details(){
      if($id = $this->input->get('user_id')){
        $result = $this->api_model->get_user_details($id);
        $result = array('result'=>true, 'return'=>$result);
      }
      else{
        $result = array('result'=>false);
      }
      echo json_encode($result);
    }
  }

?>
