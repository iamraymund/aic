<?php
	class Sign_in_orig extends CI_Controller
	{
		public function index()
		{
			/**
			*
			* Code snippet for gbox login
			*
			* @author : Teddy O. Patriarca
			* @copyright : 2016 - MIS ADNU
			* @since : September 21, 2016 Release : beta 1.0
			* @version : git / gitlab
			* @link : http://mis-git.adnu.edu.ph/tykobradyer/adnu-api
			*/

			/*
			* For debugging purposes. Kindly comment these for production
			*/

			//ini_set('display_errors', 1);
			//ini_set('display_startup_errors', 1);
			//error_reporting(E_ALL);

			// Path to the google PHP API
			require(APPPATH.'libraries/google-api-php-client-2.0.3/src/Google/Client.php');
			require(APPPATH.'libraries/google-api-php-client-2.0.3/src/Google/autoload.php');
			// require 'C:\wamp\www\AIC\application\libraries\google-api-php-client-2.0.3\vendor\autoload.php';

			$client_id = '1012905201641-6fn739e153t69v2s4br8tgtaa07nslbn.apps.googleusercontent.com'; //your client ID;
			$client_secret = 'GYXQzg3LzAUSZBFxp24Pdh6I'; //apps secret key;
			$redirect_uri = 'http://localhost/portfolio/AICProject/index.php/sign_in.php';
			$simple_api_key = ''; //leave it blank ^_^

			$client = new Google_Client();
			$client->setApplicationName("SAMPLE GBOX LOGIN");
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			$client->setRedirectUri($redirect_uri);
			$client->addScope("https://www.googleapis.com/auth/userinfo.email");

			$errMsg = "";

			$objOAuthService = new Google_Service_Oauth2($client);

			// Add Access Token to Session
			if (isset($_GET['code']))
			{
				$client->authenticate($_GET['code']);
				//$_SESSION['access_token'] =  $client->getAccessToken();
				$this->session->set_userdata('access_token', $client->getAccessToken());
				//header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
			}
			// Set Access Token to make Request
			if ($this->session->userdata('access_token'))
			{
				$client->setAccessToken($this->session->userdata('access_token'));
			}

			// Get User Data from Google and store them in $data

			if ($client->getAccessToken())
			{
				try
				{
					$userData = $objOAuthService->userinfo->get();
				}
				catch(Exception $e)
				{
					$this->load->view('sign_out');
				}

				//$_SESSION['access_token'] =  $client->getAccessToken();
				$this->session->set_userdata('access_token', $client->getAccessToken());
				$authUrl = $client->createAuthUrl();

				//APPEND THE GBOX DOMAIN SO THAT ONLY AUTHENTICATED EMAIL WITH GBOX DOMAIN WILL BE DISPLAYED IN THE SCREEN
				$authUrl = $authUrl.'&hd=gbox.adnu.edu.ph';
				if(authorize($userData))
				{
					$this->load->view('home');
					//header('Location: home.php');
				}
				else
				{
					//session_destroy();
					$this->load->view('sign_out');
				}
			}
			else
			{
				$authUrl = $client->createAuthUrl();
				$authUrl = $authUrl.'&hd=gbox.adnu.edu.ph';
			}


			// hd param can be manipulated by the client so we still need to check the returned email if that is really a gbox
			// this function also includes the api calling to retrieve who is the employee behind the given gbox
			function authorize($userData)
			{
				global $errMsg;
				if(!strpos($userData->email, '@gbox.adnu.edu.ph') || $userData->hd != 'gbox.adnu.edu.ph')
				{
					$errMsg = "Not a gbox account!";
					return false;
				}
				$email = $userData->email;


				require APPPATH.'api/src/AdnuApi.php';

				$api = new AdnuApi;
				$api->setKey('962b2d2b8e72dc6771bca613d49b46fb');
				$api->setUser('ateneoAI');
				$api->setPass('A!4dNu2k16-');
				$api->setParams(array('gbox' => $email)); //set the parameters
				$api->setUrl('https://services.adnu.edu.ph/api/v2/employee/get_employee_gbox'); //set the API url

				$return = $api->execute();
				var_dump($return);

				if(!$return || !isset($return['return']))
				{
					$errMsg = "Error";
					return false;
				}
				else if($return['values']['code'] != 0)
				{
					$errMsg = "Error! No employee with given gbox or no email supplied";
					return false;
				}
				else
				{
					//$_SESSION['logged_in'] = true;
					//$_SESSION['emp_name'] = $return['values']['data']['emp_name'] ;
					//$_SESSION['emp_id'] = $return['values']['data']['emp_id'] ;

					// adding data to session
					// $this->session->set_userdata('gbox', 'ralvarez@gbox.adnu.edu.ph');
					// $this->session->set_userdata('name', 'Raymund Edgar Alvarez');
					// $this->session->set_userdata('college', 'Computer Studies');
					// $this->session->set_userdata('department', 'Computer Science');
					// $this->session->set_userdata('position', 'Faculty');
					// $this->session->set_userdata('id_number', '201210104');

					return true;
				}
			}

			$this->load->view('header');
			$this->load->view('navigation');
			$data = array('authUrl' => $authUrl, 'errMsg' => $errMsg);
			$this->load->view('sign_in', $data);
			$this->load->view('footer');
			$this->load->view('footer_cont');
		}
	}
?>
