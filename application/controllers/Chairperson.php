<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Chairperson extends CI_Controller{
			public function __construct(){
				parent::__construct();
				$this->load->model('course_list_model');
				$this->load->model('user_courses_model');
				$this->load->model('courses_model');
				$this->load->model('chairperson_model');
			}
			public function index(){
    		// If a chairperson is logged in
				$position = $this->session->userdata('original_position');
    		if($position == 'Chairperson' || $position == 'Dean')
				{
					$this->session->set_userdata('position', 'Chairperson');
					$course_list = $this->chairperson_model->home($this->session->userdata('department_id'));
					$book_count = array();
					foreach($course_list as $row){
						$total = $this->chairperson_model->book_count($row->id);
						$latest_arr = $this->chairperson_model->book_count_latest($row->id);
						$latest = 0;
						$current_year = date("Y");
						foreach($latest_arr as $tmp){
							$latest_tmp = $this->chairperson_model->get_book_year($tmp->book_id);
							if($current_year - $latest_tmp <= 3){
								// echo "<script>console.log('$latest_tmp');</script>";
								$latest++;
							}
						}
						$book_count[$row->id] = array($total, $latest);
					}
					$data['course_list'] = $course_list;
					$data['book_count'] = $book_count;

					$this->load->view('header');
					$this->load->view('navigation_chairperson');
					$this->load->view('home_chairperson', $data);
					$this->load->view('footer');
					$this->load->view('footer_cont');
				}
				else
				{
					redirect(site_url().'/sign_out', 'refresh');
				}
    	}
			/*
			 * For Chairperson's Course List menu
			*/
			public function course_list(){
				// if logged in and chairperson
	  		if($this->session->userdata('position') == 'Chairperson')
				{
					$dept_id = $this->session->userdata('department_id');
					$college_id = $this->session->userdata('college_id');
					$this->load->view('header');
					$this->load->view('navigation_chairperson');

					$this->load->view('course_list');
					$this->load->view('footer');
					$this->load->view('course_list_scripts');
					$this->load->view('footer_cont');

					// Insert new course
					if($this->input->post('addEnd')){
						$data = array(
							'course_code' => html_escape($this->input->post('course_code')),
							'course_title' => html_escape($this->input->post('course_title')),
							'department_id' => html_escape($dept_id),
							'college_id' => html_escape($college_id)
						);
						$this->course_list_model->insert($data);
						redirect('chairperson/course-list');
					}
					// Delete course
					if($delete = html_escape($this->input->post('deleteEnd'))){
						$this->course_list_model->delete($delete);
						redirect('chairperson/course-list');
					}

					// Update course
					if($update = html_escape($this->input->post('updateEnd'))){
						$data = array(
							'course_code' => html_escape($this->input->post('course_code_update')),
							'course_title' => html_escape($this->input->post('course_title_update')),
							'department_id' => html_escape($dept_id)
						);
						$this->course_list_model->update($data, $update);
						redirect('chairperson/course-list');
					}
				}
				else
				{
					redirect(site_url().'/sign-out', 'refresh');
				}
			}

			// Get the data for the user_course that needs to be edited
			public function course_list_single(){
				if($id = html_escape($this->input->post('id'))){
					echo json_encode($this->course_list_model->get($id));
				}
			}

			public function course_list_select_courses(){
				echo json_encode($this->course_list_model->select($this->session->userdata('department_id')));
			}


			/*
			 * For Chairperson's Course Asssignment menu
			*/
			public function user_courses(){
				// if logged in and chairperson
	  		if($this->session->userdata('position') == 'Chairperson')
				{
					$dept_id = $this->session->userdata('department_id');
					$this->load->view('header');
					$this->load->view('navigation_chairperson');

					// get existing user courses
	        $data['courses'] = $this->user_courses_model->get_all($dept_id);

					$this->load->view('user_courses', $data);
					$this->load->view('footer');
					$this->load->view('user_courses_scripts'); // custom scripts
					$this->load->view('footer_cont');

					if($this->input->post('addEnd')){
						$gbox = html_escape($this->input->post('gbox'));
						$courses = html_escape($this->input->post('courses'));
						$data = array(
							'gbox' => $gbox,
							'department_id' => $dept_id
						);
						$this->user_courses_model->insert($data, $courses);
						redirect('chairperson/user-courses');
					}

					// Delete course
					if($delete = html_escape($this->input->post('deleteEnd'))){
						$user_id = $this->courses_model->get_id($this->session->userdata('gbox'));
						$this->user_courses_model->delete($delete, $user_id);
						redirect('chairperson/user-courses');
					}

					// Update course
					if($update = html_escape($this->input->post('updateEnd'))){
						$user_course_arr = $this->user_courses_model->get_old_list($update);
						$old_list = explode(',', $user_course_arr->course_list);
						$new_list = explode(',', $this->input->post('courses_update'));

						// search for deleted course in old course list
						$missing = array();
						for($i = 0; $i < sizeof($old_list); $i++){
							$flag = false;
							for($j = 0; $j < sizeof($new_list); $j++){
								if($old_list[$i] == $new_list[$j]){
									$flag = true;
									break;
								}
							}
							if(!$flag){
								array_push($missing, $old_list[$i]);
							}
						}
						// $gbox = $this->session->userdata('gbox');
						$user_id = $user_course_arr->user_id;
						// echo "<script>console.log('$user_id');</script>";
						for($i=0; $i<sizeof($missing); $i++){
							// Get course id
							$course_id = $this->courses_model->get_course_id($missing[$i]);
							// echo "<script>console.log('$course_id');</script>";
							$this->user_courses_model->delete_course_removed($user_id, $course_id);
						}

						$data = array(
							'course_list' => html_escape($this->input->post('courses_update')),
							'gbox' => html_escape($this->input->post('gbox_update'))
						);
						$this->user_courses_model->update($data, $update);
						redirect('chairperson/user-courses');
					}
				}
				else
				{
					redirect(site_url().'/sign-out', 'refresh');
				}
			}

			// Get the data for the user courses that needs to be edited
			public function user_courses_single(){
				if($id = html_escape($this->input->post('id'))){;
					echo json_encode($this->user_courses_model->get($id));
				}
			}

			public function user_courses_select_user_courses(){
				echo json_encode($this->user_courses_model->select($this->session->userdata('department_id')));
			}
			/*
			 * For Chairperson's Reports menu
			*/
			public function reports(){
				// if logged in and chairperson
				if($this->session->userdata('position') == 'Chairperson')
				{
					$dept_id = $this->session->userdata('department_id');
					$this->load->view('header');
					$this->load->view('navigation_chairperson');
					$this->load->view('reports_chairperson');
					$this->load->view('footer');
					$this->load->view('reports_chairperson_scripts');
					$this->load->view('footer_cont');

				}
			}
			public function get_reports(){
				$course_list = $this->chairperson_model->home($this->session->userdata('department_id'));
				$book_courses = array();
				// traverse each course
				foreach($course_list as $row){
					$assigned_book = $this->chairperson_model->book_count_latest($row->id);
					$flag = false;
					foreach($assigned_book as $tmp){
						$book_details = $this->chairperson_model->get_book_details($tmp->book_id);
						if(!$flag){
							array_push($book_courses, array(
								'course_code'=>$row->course_code,
								'course_title'=>$row->course_title,
								'title'=>$book_details->title,
								'authors'=>$book_details->authors,
								'year'=>$book_details->year,
								'edition'=>$book_details->edition,
								'summary'=>$book_details->summary,
								'publication_info'=>$book_details->publication_info,
								'ISBN'=>$book_details->ISBN,
								'length'=>$book_details->length,
								'contributors'=>$book_details->contributors));
							$flag = true;
						}
						else{
							array_push($book_courses, array(
								'title'=>$book_details->title,
								'authors'=>$book_details->authors,
								'year'=>$book_details->year,
								'edition'=>$book_details->edition,
								'summary'=>$book_details->summary,
								'publication_info'=>$book_details->publication_info,
								'ISBN'=>$book_details->ISBN,
								'length'=>$book_details->length,
								'contributors'=>$book_details->contributors));
						}
					}
				}
				echo json_encode($book_courses);
			}

		}
?>
