<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Home extends CI_Controller{
      public function __construct(){
				parent::__construct();
				$this->load->model('user_courses_model');
				$this->load->model('home_model');
			}
    	public function index(){
        /**
        *
        * Code snippet for gbox login
        *
        * @author : Teddy O. Patriarca
        * @copyright : 2016 - MIS ADNU
        * @since : September 21, 2016 Release : beta 1.0
        * @version : git / gitlab
        * @link : http://mis-git.adnu.edu.ph/tykobradyer/adnu-api
        */

        // Path to the google PHP API
        require(APPPATH.'libraries/google-api-php-client-2.1.1/src/Google/Client.php');
        require(APPPATH.'libraries/google-api-php-client-2.1.1/vendor/autoload.php');

        $client_id = '1012905201641-6fn739e153t69v2s4br8tgtaa07nslbn.apps.googleusercontent.com'; //your client ID;
        $client_secret = 'GYXQzg3LzAUSZBFxp24Pdh6I'; //apps secret key;
        // For the website
        //$redirect_uri = 'http://localhost/iamraymund/portfolio/AICProject/index.php/home';

        // For localhost
        $redirect_uri = 'http://www.iamraymund.com/portfolio/AICProject/index.php/home';
        $simple_api_key = ''; //leave it blank ^_^

        $client = new Google_Client();
        $client->setApplicationName("SAMPLE GBOX LOGIN");
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("https://www.googleapis.com/auth/userinfo.email");

        $errMsg = "";

        $objOAuthService = new Google_Service_Oauth2($client);

        // Add Access Token to Session
        if (isset($_GET['code']))
        {
          $client->authenticate($_GET['code']);
          $this->session->set_userdata('access_token', $client->getAccessToken());
        }
        // Set Access Token to make Request
        if ($this->session->userdata('access_token'))
        {
          $client->setAccessToken($this->session->userdata('access_token'));
        }

        // Get User Data from Google and store them in $data
        if ($client->getAccessToken())
        {
          try
          {
          	$userData = $objOAuthService->userinfo->get();
          }
          catch(Exception $e)
          {
            $this->session->set_flashdata('sign_in', 'token_error');
            redirect(site_url() . '/home', 'refresh');
          }

          $this->session->set_userdata('access_token', $client->getAccessToken());
          $authUrl = $client->createAuthUrl();

          //APPEND THE GBOX DOMAIN SO THAT ONLY AUTHENTICATED EMAIL WITH GBOX DOMAIN WILL BE
          //DISPLAYED IN THE SCREEN
          $authUrl = $authUrl.'&hd=gbox.adnu.edu.ph';
          $this->authorize($userData);
        }
        else
        {
          $authUrl = $client->createAuthUrl();
          $authUrl = $authUrl.'&hd=gbox.adnu.edu.ph';
        }

        $data['auth'] = $authUrl;

    		$this->load->view('header');
  			$this->load->view('navigation', $data);
  			$this->load->view('home');
        $this->load->view('footer');
        $this->load->view('footer_cont');
    	}

      // hd param can be manipulated by the client so we still need to check the returned email if that is really a gbox
      // this function also includes the api calling to retrieve who is the employee behind the given gbox
      public function authorize($userData)
      {
        global $errMsg;
        if(!strpos($userData->email, '@gbox.adnu.edu.ph') || $userData->hd != 'gbox.adnu.edu.ph')
        {
          $this->session->set_flashdata('sign_in', 'gbox_error');
          return false;
        }
        $email = $userData->email;

        require APPPATH.'third_party/api/src/AdnuApi.php';

        // API Request for Employee ID using GBOX
        $api = new AdnuApi;
        $api->setKey('962b2d2b8e72dc6771bca613d49b46fb');
        $api->setUser('ateneoAI');
        $api->setPass('A!4dNu2k16-');
        $api->setParams(array('gbox' => $email)); //set the parameters
        $api->setUrl('https://services.adnu.edu.ph/api/v2-dev/employee/employee_id_by_gbox/'); //set the API url
        $return = $api->execute(); // get the employee id

        if(!$return || !isset($return['return']))
        {
          $errMsg = "Error";
          $this->session->set_flashdata('sign_in', 'api_error');

          return false;
        }
        else if($return['values']['code'] != 0)
        {
          $errMsg = "Error! No employee with given gbox or no email supplied";
          $this->session->set_flashdata('sign_in', 'employee_error');

          return false;
        }
        else
        {
          $emp_name = $return['values']['data']['emp_name'];
          $emp_id = $return['values']['data']['emp_id'];

          // API Request for Employee Designation
          $api->setParams(array('emp_id' => $emp_id)); //set the parameters
          $api->setUrl('https://services.adnu.edu.ph/api/v2-dev/employee/employee_designation'); //set the API url
          $return = $api->execute(); // get the employee designation

          $data_size =  sizeof($return['values']['data']); // 2 = Dean, 1 = Chair / Faculty
          $job_title = $return['values']['data'][0]['job_title'];
          $designationtype_desc = $return['values']['data'][0]['designationtype_desc']; // deans, faculty, chair
          // For Dean - API already supported the dept & college of dean
          if($data_size == 2){
            $dept_name = $return['values']['data'][1]['dept_name'];
            $college_name = $return['values']['data'][0]['dept_name'];
          }
          // For Chair and Faculty - API NOT YET supported the college name
          else{
            $dept_name = $return['values']['data'][0]['dept_name'];
            // NOTE: Set college name for all chair / faculty to College of Computer Studies (regardless of their actual college)
            $college_name = "College of Computer Studies - Dean's Office";
          }
          $this->session->set_userdata('job_title', $job_title);
          $this->session->set_userdata('designationtype_desc', $designationtype_desc);

          // Debug some returned data
          echo "<script>console.log('Job Title: $job_title');</script>";
          echo "<script>console.log('Designation: $designationtype_desc');</script>";
          echo "<script>console.log('Department Name: $dept_name');</script>";

          $result = $this->home_model->get_dept_info($dept_name);
          // If department is already existing
          if(sizeof($result)){
            $dept_id = $result->id;
            $college_id = $result->college_id;
          }
          // otherwise, add the department in the database
          else{
            $this->home_model->add_dept_info($dept_name, $college_name);
            $result = $this->home_model->get_dept_info($dept_name);
            $dept_id = $result->id;
            $college_id = $result->college_id;
          }

          // adding data to session
          $this->session->set_userdata('gbox', $email);
          $this->session->set_userdata('name', $emp_name);
          $this->session->set_userdata('id_number', $emp_id);
          $this->session->set_userdata('department_id', $dept_id);
          $this->session->set_userdata('college_id', $college_id);

          $data = array(
            'id_number' => $emp_id,
            'gbox' => $email,
            'full_name' => $emp_name,
            'department_id' => $dept_id,
            'college_id' => $college_id
          );
          // add new user if not exist, otherwise update its info
          $this->user_courses_model->insert_no_courses($data);

          if($designationtype_desc == 'Deans'){
      			$this->session->set_userdata('position', 'Dean');
      			$this->session->set_userdata('original_position', 'Dean');

            redirect(site_url().'/dean', 'refresh');
          }
          else if($designationtype_desc == 'Chair'){
      			$this->session->set_userdata('position', 'Chairperson');
      			$this->session->set_userdata('original_position', 'Chairperson');

            redirect(site_url().'/chairperson', 'refresh');
          }
          else if($designationtype_desc == 'Faculty'){
      			$this->session->set_userdata('position', 'Faculty');
      			$this->session->set_userdata('original_position', 'Faculty');

            redirect(site_url().'/faculty', 'refresh');
          }

          return true;
        }
      }
    }
?>
