<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User_courses_model extends CI_Model {
		public function __construct(){
			parent::__construct();
		}

		public function get_all($id){
			$this->db->where('department_id', $id);
			$this->db->order_by("course_title", "asc");
			$query = $this->db->get('courses');
			return $query->result();
		}

		public function get($id){
			$this->db->select('users.gbox, user_courses.course_list');
			$this->db->from('user_courses');
			$this->db->join('users', 'users.id = user_courses.user_id');
			$this->db->where('user_courses.id', $id);
			$query = $this->db->get();
			return $query->row();
		}

		public function select($dept_id){
			$this->db->select('users.full_name, user_courses.id, users.gbox, user_courses.course_list');
			$this->db->from('users');
			$this->db->where('department_id', $dept_id);
			$this->db->join('user_courses', 'users.id = user_courses.user_id');
			$query = $this->db->get();
			return $query->result();
		}
		public function insert_no_courses($data){
			$this->db->where('gbox', $data['gbox']);
			$query = $this->db->get('users');
			$num_rows = $query->num_rows();

			// if not yet found in 'users' table
			if($num_rows == 0){
				$this->db->insert('users', $data);
				$last_id = $this->db->insert_id();
				$this->db->insert('user_courses', array('user_id'=>$last_id));
			}
			// otherwise, update the user
			else{
				$this->db->set($data);
				$this->db->where('gbox', $data['gbox']);
				$this->db->update('users', $data);
			}
		}
		public function insert($data, $courses){
			$this->db->insert('users', $data);
			$last_id = $this->db->insert_id();
			$this->db->insert('user_courses', array('user_id'=>$last_id, 'course_list'=>$courses));

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('add', 'success');
			}
			else{
				$this->session->set_flashdata('add', 'error');
			}
		}

		public function get_old_list($id){
			$this->db->where('id', $id);
			$query = $this->db->get('user_courses');
			return $query->row();
		}
		public function delete_course_removed($user_id, $course_id){
			$this->db->where(array('user_id' => $user_id, 'course_id' => $course_id));
			$this->db->delete('book_courses');
		}
		public function update($data, $id){
			// Update course_list column
			// $this->db->where('id', $id);
			// $query = $this->db->get('user_courses');
			// $result = $query->row();
			//
			// $old_list = $result->course_list;
			// $new_list = $data['course_list'];

			$this->db->set('course_list', $data['course_list']);
			$this->db->where('id', $id);
			$this->db->update('user_courses');

			// Remove book_course connection to the course remove from the user

			$this->session->set_flashdata('update', 'success');

			// Get the user_id for gbox update
			$this->db->where('id', $id);
			$query = $this->db->get('user_courses');
			$result = $query->row();

			// Update gbox column
			$this->db->set('gbox', $data['gbox']);
			$this->db->where('id', $result->user_id);
			$this->db->update('users');

		}

		public function delete($id, $user_id){
			$this->db->where('id', $id);
			$query = $this->db->get('user_courses');
			$result = ($query->row())->user_id;

			if($result != $user_id){
				$this->session->set_flashdata('delete', 'success');
				$this->db->delete('user_courses', 'id = ' . $id);
				$this->db->delete('users', 'id = '. $result);
			}
			else{
				$this->session->set_flashdata('delete', 'error');
			}
		}
	}
?>
