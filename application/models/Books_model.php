<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Books_model extends CI_Model {
		public function select_books(){
			$query = $this->db->get('books');
			return $query->result();
		}
	}
?>
