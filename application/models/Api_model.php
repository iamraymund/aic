<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Api_model extends CI_Model{
    public function get_book_details($id){
      $this->db->select('title, authors, year, edition, summary, publication_info, ISBN, length, contributors, created_by, last_modified_by');
      $this->db->from('books');
      $this->db->where('id', $id);
      $query = $this->db->get();
      return $query->row();
    }
    public function get_book_courses($code){
      $this->db->select('id');
      $this->db->where('course_code', $code);
      $query = $this->db->get('courses');
      if($query->num_rows() > 0){
        $id = ($query->row())->id;
        $this->db->select('book_id');
        $this->db->where('course_id', $id);
        $query = $this->db->get('book_courses');
        return $query->result();
      }
      return false;
    }
    public function get_user_courses_gbox($gbox){
      $this->db->select('id');
      $this->db->where('gbox', $gbox);
      $query = $this->db->get('users');
      if($query->num_rows() > 0){
        $id = ($query->row())->id;
        $this->db->select('course_list');
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_courses');
        return ($query->row())->course_list;
      }
      return false;
    }
    public function get_user_courses_id($id){
      $this->db->select('id');
      $this->db->where('id_number', $id);
      $query = $this->db->get('users');
      if($query->num_rows() > 0){
        $id = ($query->row())->id;
        $this->db->select('course_list');
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_courses');
        return ($query->row())->course_list;
      }
      return false;

    }
    public function get_user_details($id){
      $this->db->select('users.id_number, users.gbox, users.full_name, departments.dept_name, colleges.college_name');
      $this->db->from('users');
      $this->db->join('departments', 'departments.id = users.department_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->where('users.id', $id);
      $query = $this->db->get();
      return $query->row();
    }
  }
?>
