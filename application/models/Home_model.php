<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Home_model extends CI_Model{
    public function get_dept_info($dept_name){
      $this->db->from('departments');
      $this->db->where('dept_name', $dept_name);
      $query = $this->db->get();
      return $query->row();
    }
    public function add_dept_info($dept_name, $college_name){
      // RECOMMENDATION: The college name is hard-coded in the database (for now). Just include college name in API not just in Deans but chairperson & faculty too
      $college = array(
        "College of Business and Accountancy - Dean's Office" => 1,
        "College of Computer Studies - Dean's Office" => 2,
        "College of Humanities and Social Sciences - Dean's Office" => 3,
        "College of Nursing - Dean's Office" => 4,
        "College of Science and Engineering - Dean's Office" => 5
      );
      $this->db->insert('departments', array('dept_name' => $dept_name, 'college_id' => $college[$college_name]));
    }
  }
?>
