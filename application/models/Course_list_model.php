<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Course_list_model extends CI_Model {

		public function __construct(){
			parent::__construct();
		}
		public function get($id){
			$this->db->where('id', $id);
			$query = $this->db->get('courses');
			return $query->result();
		}
		public function select($dept_id){
			$this->db->where('department_id', $dept_id);
			$query = $this->db->get('courses');
			return $query->result();
		}

		public function insert($data){
			$this->db->insert('courses', $data);
			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('add', 'success');
			}
			else{
				$this->session->set_flashdata('add', 'error');
			}
		}

		public function update($data, $id){
			$this->db->set($data);
			$this->db->where('id', $id);
			$this->db->update('courses', $data);

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('update', 'success');
			}
			else{
				$this->session->set_flashdata('update', 'error');
			}
		}

		public function delete($id){
			$this->db->delete('courses', "id = $id");
			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('delete', 'success');
			}
			else{
				$this->session->set_flashdata('delete', 'error');
			}
		}


	}
?>
