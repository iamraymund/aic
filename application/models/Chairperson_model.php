<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Chairperson_model extends CI_Model {
		public function home($dept_id){
			$this->db->where('department_id', $dept_id);
			$this->db->order_by('course_title', 'asc');
			$query = $this->db->get('courses');
			return $query->result();
		}
		public function book_count($id){
			$this->db->where('course_id', $id);
			$query = $this->db->get('book_courses');
			return $query->num_rows();
		}
		public function book_count_latest($id){
			$this->db->where('course_id', $id);
			$query = $this->db->get('book_courses');
			return $query->result();
		}
		public function get_book_details($id){
			$this->db->where('id', $id);
			$query = $this->db->get('books');
			return $query->row();
		}
		public function get_book_year($id){
			$this->db->where('id', $id);
			$query = $this->db->get('books');
			return ($query->row())->year;
		}

	}
?>
