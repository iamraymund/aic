<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Courses_model extends CI_Model {

		public function __construct(){
			parent::__construct();
		}
		/*
		 * For Book Count Badge
		 */
		public function get_course_id($course_code){
			$this->db->select('id');
			$this->db->where('course_code', $course_code);
			$query = $this->db->get('courses');
			return ($query->row())->id;
		}
		public function book_count($id){
			$this->db->where('course_id', $id);
			$query = $this->db->get('book_courses');
			return $query->num_rows();
		}
		public function book_count_latest($id){
			$this->db->where('course_id', $id);
			$query = $this->db->get('book_courses');
			return $query->result();
		}
		public function get_book_year($id){
			$this->db->where('id', $id);
			$query = $this->db->get('books');
			return ($query->row())->year;
		}
		/* ./ For Book Count Badge */

		public function get($id){
			$this->db->where('id', $id);
			$query = $this->db->get('books');
			return $query->row();
		}
		// Get the user id
		public function get_id($gbox){
			$this->db->select('id');
			$this->db->where('gbox', $gbox);
			$query = $this->db->get('users');
			return ($query->row())->id;
		}
		public function select_course($id){
			$this->db->select('course_list');
			$this->db->where('user_id', $id);
			$query = $this->db->get('user_courses');
			return ($query->row())->course_list;
		}
		public function select_books($code){
			$this->db->select('id');
			$this->db->where('course_code', $code);
			$query = $this->db->get('courses');
			$course_id = ($query->row())->id;
			//$user_id = $this->session->userdata('user_id');

			// NOTE:20 Get all books under specific course regardless who the users
			$this->db->select('books.id, books.title, books.authors, books.year, books.edition, books.summary, books.publication_info, books.ISBN, books.length, books.contributors');
			$this->db->from('books');
			$this->db->join('book_courses', 'books.id = book_courses.book_id');
			//$this->db->where('book_courses.user_id', $user_id);
			$this->db->where('book_courses.course_id', $course_id);
			$query = $this->db->get();
			return $query->result();
		}
		public function map_course_code($arr){
			$this->db->from('courses');
			$this->db->where_in('course_code', $arr);
			$query = $this->db->get();
			return $query->result();
		}

		public function insert($data, $user_id, $course_code){
			$this->db->select('id');
			$this->db->where('course_code', $course_code);
			$query = $this->db->get('courses');
			$course_id = ($query->row())->id;

			$this->db->insert('books', $data);
			$last_id = $this->db->insert_id();

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('add', 'success');
			}
			else{
				$this->session->set_flashdata('add', 'error');
			}

			$this->db->insert('book_courses', array('user_id'=>$user_id, 'course_id'=>$course_id, 'book_id'=>$last_id));
		}

		public function insert_existing($user_id, $course_code, $book_id){
			$this->db->select('id');
			$this->db->where('course_code', $course_code);
			$query = $this->db->get('courses');
			$course_id = ($query->row())->id;

			$this->db->insert('book_courses', array('user_id'=>$user_id, 'course_id'=>$course_id, 'book_id'=>$book_id));
			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('add', 'success');
			}
			else{
				$this->session->set_flashdata('add', 'error');
			}
		}

		public function update($data, $id){
			$this->db->set($data);
			$this->db->where('id', $id);
			$this->db->update('books', $data);

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('update', 'success');
			}
			else{
				$this->session->set_flashdata('update', 'error');
			}
		}

		public function delete($book_id, $user_id, $course_code){

			$this->db->select('id');
			$this->db->where('course_code', $course_code);
			$query = $this->db->get('courses');
			$course_id = ($query->row())->id;

			$this->db->where(array('user_id'=>$user_id, 'course_id'=>$course_id, 'book_id'=>$book_id));
			$this->db->delete('book_courses');

			if($this->db->affected_rows() > 0){
				$this->session->set_flashdata('delete', 'success');
			}
			else{
				$this->session->set_flashdata('delete', 'error');
			}

			// DONE:190 Only delete book if no other courses referencing the book
			// $this->db->where('book_id', $book_id);
			$this->db->where('book_id', $book_id);
			$query = $this->db->get('book_courses');
			$num_rows = $query->num_rows();
			if($num_rows == 0){
				$this->db->delete('books','id = ' .$book_id);
			}
		}
	}
?>
