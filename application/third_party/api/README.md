<h1>AdNU API CLIENT</h1>
<p>
A simple PHP library used to connect to ADNU API Server and a code snippet to use the 
Google Oauth2 login using gbox account<br/><br/>
Version : 1.0 beta <br/>
Copyright : 2016 - AdNU MIS <br/>
Author : Teddy O. Patriarca <br/>
</p>

<hr/>

<h3>Requirements</h3>
<ul>
	<li>PHP 5 or later</li>
	<li>php_curl</li>
	<li>Google PHP API Client</li>
	<li>ADNU API Secret Key</li>
</ul>

<hr/>

<h3>Installation</h3>
<ul>
	<li>Clone or <a href="http://mis-git.adnu.edu.ph/tykobradyer/adnu-api/repository/archive.zip?ref=master">Download</a> the release</li>
	<li>Clone or <a href="https://github.com/google/google-api-php-client">Download</a> Google PHP API Client</li>
	<li>Create a directory "libraries" in the root folder</li>
	<li>Paste the Google PHP API in the libraries</li>
	<li>
		Point the Client.php and autoload.php (see examples/example2.php)
		<pre>
			require __DIR__ . '/path/to/your-project/libraries/google-api-php-client/src/Google/Client.php';
			require __DIR__ . '/path/to/your-project/libraries/google-api-php-client/vendor/autoload.php';
		</pre>
	</li>
	<li>Create API keys (<a href="https://www.formget.com/codeigniter-google-oauth-php/">Refer to this tutorial</a>)</li>
	<li>Paste your client ID and secret key on the variable specified (see examples/example2.php)</li>
</ul>

<hr/>

<h3>API Client Methods</h3>
<pre>setKey(string $key)</pre>
<p>Set the API key</p>

<br/>

<pre>setUser(string $user)</pre>
<p>Set the HTTP Authentication Username</p>

<br/>

<pre>setPass(string $pass)</pre>
<p>Set the HTTP Authentication Password</p>

<br/>

<pre>setParams(array $params)</pre>
<p>Set the POST parameters</p>

<br/>

<pre>setUrl(string $url)</pre>
<p>Set the API URL</p>
<p><i>Note: You can specify the API return format (xml, json) by appending <code>/format/json</code> or <code>/format/xml</code></i></p>
<br/>

<pre>setProxy(string $proxyAndPort)</pre>
<p>Set the proxy and port if needed</p>
<p>The format is: <code>proxyip:port</code> </p>

<br/>

<pre>execute()</pre>
<p>Perform the API calling</p>
<p>returns <code>API result in JSON or XML format if success and boolean false if failed</code></p>

<hr/>

<h3>Changelog</h3>
<h5>Version 1.0</h5>
<ul>
	<li>Initial Implementation</li>
</ul>

<p>
	For questions, concerns and bug, kindly send an email to me <code>tpatriarca@gbox.adnu.edu.ph</code>
</p>

<p>
	Good Day and happy coding! ^_^
</p>