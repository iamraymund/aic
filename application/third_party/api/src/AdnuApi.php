<?php

/**
* AdnuApi - A simple PHP library used to connect to ADNU API Server
*
* PHP version 5
*
* @author : Teddy O. Patriarca
* @copyright : 2016 - MIS ADNU
* @since : September 21, 2016 Release : beta 1.0
* @version : git / gitlab
* @link : http://mis-git.adnu.edu.ph/tykobradyer/adnu-api
*
*/

define("SYSTEM_NAME", "ADNU API");

class AdnuApi
{
	private static $private_key = ""; //your private key here
	private static $username = "";
	private static $password = "";

	private $params = array();
	private $url = "";

	private $proxy;
	//curl
	private $ch;

	function __construct()
	{
		if(!function_exists('curl_version'))
		{
			echo SYSTEM_NAME . " requires php curl extension to be loaded!";
			exit;
		}

		// check the cacert
		if(!file_exists(__DIR__ . "/cacert.pem"))
		{
			echo "cacert.pem not found!";
		}
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->ch, CURLOPT_CAINFO, dirname(__FILE__)."/cacert.pem");
		// curl_setopt($this->ch, CURLOPT_SSLVERSION,3);
	}

	public function setKey($key)
	{
		self::$private_key = $key;
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			'X-API-KEY: '.self::$private_key.'',
  		));
	}

	public function setUser($user)
	{
		self::$username = $user;
	}

	public function setPass($pass)
	{
		self::$password = $pass;
	}

	/**
	* Sets the parameter needed
	*
	* @param : $params - array
	* @return : void
	*
	*/

	public function setParams($params = array())
	{
		//format the array to concatenate it to post parameters
		$fields_string = "";

		foreach($params as $key => $value) {
		  $fields_string .= $key.'='.$value.'&';
		}

		$this->params = rtrim($fields_string, "&");
		curl_setopt($this->ch,CURLOPT_POST, count($params));
		curl_setopt($this->ch,CURLOPT_POSTFIELDS, $this->params);
	}

	public function setUrl($url)
	{
		$this->url = $url;
		curl_setopt($this->ch, CURLOPT_URL, $this->url);
	}

	public function setProxy($proxy)
	{
		$this->proxy = $proxy;
		curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy);
	}

	public function execute()
	{
		curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($this->ch, CURLOPT_USERPWD, self::$username . ':' . self::$password);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER,0);

		$exec = curl_exec($this->ch);

		if ($exec === false)
		{
			return array('return' => false, 'msg' => curl_error($this->ch));
		}
		return array('return' => true, 'values' => json_decode($exec, true));
	}
}
?>
